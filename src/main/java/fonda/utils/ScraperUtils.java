package fonda.utils;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by ofekrom on 13/12/2016.
 */
public class ScraperUtils {


    public static String getTextForArea(Elements dataTable, String alternateApartmentSize) {
        String result = getTextFromddOrErrorMessage(Constants.LIVING_AREA_ELEMENT, dataTable);
        if (result.contains("not available")) {
            result = alternateApartmentSize;
        }
        return result;
    }

    public static Integer getConstructionYear(Elements dataTable, Integer alternateConstructionYear) {
        Integer result = getIntegerFromddOrNegative(Constants.YEAR_OF_CONSTRUCTION, dataTable);
        if (result == -1) {
            result = alternateConstructionYear;
        }
        return result;
    }

    public static String getTextFromddOrErrorMessage(String dtValue, Elements dataTable) {
        String result;
        try {
            result = dataTable.select("dt:contains(" + dtValue + ") + dd").first().text();
        } catch (NullPointerException exception) {
            result = dtValue + " is not available";
        }
        return result;
    }

    public static Integer getIntegerFromddOrNegative(String dtValue, Elements dataTable) {
        Integer result;
        try {
            result = Utils.extractFirstNumberFromString(dataTable.select("dt:contains(" + dtValue + ") + dd").first().text());
        } catch (Exception exception) {
            result = -1;
        }
        return result;
    }


    public static String getTextFromddOrNull(String dtValue, Elements dataTable) {
        String result = null;
        try {
            result = dataTable.select("dt:contains(" + dtValue + ") + dd").first().text();
        } catch (NullPointerException exception) {

        }
        return result;
    }

    public static Integer calculateListedSince(Elements dataTable) {
        String listedSince = ScraperUtils.getTextFromddOrNull(Constants.LISTED_SINCE_ELEMENT, dataTable);
        Integer retValue = -1;
        DateFormat dateFormat = DateFormat.getDateInstance();
        if (listedSince != null) {
            if (listedSince.contains("today")) {
                retValue = 0;
            } else {
                try {
                    Date date = dateFormat.parse(listedSince);
                    retValue = (int) ((System.currentTimeMillis() - date.getTime()) / Constants.DAY_IN_MILIS);
                } catch (ParseException e) {
                }
                if (retValue == -1) {
                    try {
                        retValue = Utils.extractFirstNumberFromString(listedSince);
                    } catch (Exception e) {
                        retValue = -1;
                    }
                    if (retValue > 0) {
                        if (listedSince.contains("m")) {
                            retValue = 30 * retValue;
                        } else if (listedSince.contains("w")) {
                            retValue = 7 * retValue;
                        }
                    }
                }
            }
        }
        return retValue;
    }

    public static Integer getIntegersFromddOrNegative(String dtValue, Elements dataTable) {
        Integer result;
        try {
            result = Utils.extractRealAllNumbersFromString((dataTable.select("dt:contains(" + dtValue + ") + dd").first().text()));
        } catch (Exception exception) {
            result = -1;
        }
        return result;
    }

    public static int[] calculateNumberOfRoomsAndBedRooms(Elements dataTable) {
        int totalRooms = ScraperUtils.getIntegersFromddOrNegative(Constants.NUMBER_OF_ROOMS, dataTable);
        int numberOfRooms, numberOfBedRooms;
        if (totalRooms >= 10) {
            numberOfRooms = totalRooms / 10;
            numberOfBedRooms = totalRooms % 10;
        } else {
            numberOfRooms = totalRooms;
            numberOfBedRooms = -1;
        }
        return new int[]{numberOfRooms, numberOfBedRooms};
    }

    public static boolean isAssetAnApartment(Document doc) {
        Elements dataTable = doc.select(".object-kenmerken-list");
        return !isAssetLand(dataTable) && !isAssetOther(dataTable);
    }

    private static boolean isAssetLand(Elements dataTable) {
        String assetLand = ScraperUtils.getTextFromddOrNull(Constants.TYPE_OF_PROPERTY, dataTable);
        return assetLand != null && assetLand.contains("plot");
    }

    private static boolean isAssetParking(Elements dataTable) {
        return ScraperUtils.getTextFromddOrNull(Constants.TYPE_OF_PARKING_FACILITIES, dataTable) != null;
    }

    private static boolean isAssetOther(Elements dataTable) {
        return ScraperUtils.getTextFromddOrNull(Constants.TYPE_OF_OTHER_PROPERTY, dataTable) != null;
    }
}


