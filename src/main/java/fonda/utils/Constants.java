package fonda.utils;

import java.util.concurrent.TimeUnit;

/**
 * Created by ofekrom on 15/11/2016.
 */
public class Constants {
    public static final String FUNDA_BASE_URL = "http://www.funda.nl";
    public static final String FUNDA_SALE_SEARCH_URL = "/en/koop/amsterdam/+2km/150000-1500000/";
    public static final String FUNDA_RENT_SEARCH_URL = "/en/huur/amsterdam/+2km/500-10000/";

    public static final String SALE_PRICE_ELEMENT = "Asking price";
    public static final String RENTAL_PRICE_ELEMENT = "Rental price";
    public static final String LIVING_AREA_ELEMENT = "Living area";
    public static final String AREA_ELEMENT = "Area";
    public static final String STATUS_ELEMENT = "Status";
    public static final String LISTED_SINCE_ELEMENT = "Listed since";
    public static final String YEAR_OF_CONSTRUCTION = "Year of construction";
    public static final String YEAR_OF_CONSTRUCTION_ALTERNATE = "Construction period";
    public static final String NUMBER_OF_ROOMS = "Number of rooms";
    public static final String TYPE_OF_PROPERTY = "Type of property";
    public static final String TYPE_OF_OTHER_PROPERTY = "Type of other property";
    public static final String TYPE_OF_PARKING_FACILITIES = "Type of parking facilities";

    public static final long DAY_IN_MILIS = TimeUnit.MILLISECONDS.convert(1, TimeUnit.DAYS);
}
