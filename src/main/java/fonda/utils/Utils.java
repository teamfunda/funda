package fonda.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ofekrom on 30/11/2016.
 */
public class Utils {
    public static int extractFirstNumberFromString(String s) {
        try {
            int i = 0;
            while (i < s.length() && !Character.isDigit(s.charAt(i))) {
                i++;
            }
            int j = i + 1;
            while (j < s.length() && (Character.isDigit(s.charAt(j)) || s.charAt(j) == ',')) {
                j++;
            }
            String temp = s.substring(i, j).replaceAll("\\D+", "");
            return Integer.valueOf(temp);

//            s = s.replaceAll("\\D+", "");
//            return Integer.valueOf(s);
        } catch (Exception e) {

        }
        return 0;
    }

    public static Integer extractAllNumbersFromString(String s) {
        try {
            Matcher matcher = Pattern.compile("\\d+").matcher(s);
            matcher.find();
            return Integer.valueOf(matcher.group());
        } catch (Exception e) {
            return -1;
        }
     }


    public static Integer extractRealAllNumbersFromString(String s) {
        try {
            return Integer.parseInt(s.replaceAll("[\\D]", ""));
        } catch (Exception e) {
            return -1;
        }
    }

}
