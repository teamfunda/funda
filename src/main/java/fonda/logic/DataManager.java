package fonda.logic;

import fonda.controller.AssetController;
import fonda.pojo.Asset;
import fonda.utils.Constants;
import fonda.utils.ScraperUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ofekrom on 03/12/2016.
 */
public class DataManager {

    private AssetController assetController;

    public DataManager(ApplicationContext context) {
        this.assetController = context.getBean(AssetController.class);
    }

    public void printAverageRentPriceDesc() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForRent();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        /*
        divide assets to lists by group zip code and put them in the map
         */
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        int sumRent = 0;
        Map<String, Integer> averagePriceMap = new TreeMap<String, Integer>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumRent += asset.getRentPrice();
            }
            averagePriceMap.put(key, sumRent / rentAssetsByShortZipCode.get(key).size());
            sumRent = 0;
        }

        System.out.println("Average rent prices:\n");
        for (String key : averagePriceMap.keySet()) {
            System.out.println("Short zip code: " + key);
            System.out.println("Average rent price: " + averagePriceMap.get(key) + "\n");
        }
    }

    public void printAverageSalePriceDesc() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForSale();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        int sumRent = 0;
        Map<String, Integer> averagePriceMap = new TreeMap<String, Integer>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumRent += asset.getSalePrice();
            }
            averagePriceMap.put(key, sumRent / rentAssetsByShortZipCode.get(key).size());
            sumRent = 0;
        }

        System.out.println("Average sale prices:\n");
        for (String key : averagePriceMap.keySet()) {
            System.out.println("Short zip code: " + key);
            System.out.println("Average sale price: " + averagePriceMap.get(key) + "\n");
        }
    }

    public void printAverageRentPricePerMeter() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForRent();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        double sumRentPerMeter = 0;
        Map<String, Double> averagePriceMap = new TreeMap<String, Double>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumRentPerMeter += asset.getRentPrice() / asset.getSize();
            }
            averagePriceMap.put(key, sumRentPerMeter / rentAssetsByShortZipCode.get(key).size());
            sumRentPerMeter = 0;
        }

        System.out.println("Average rent prices per meter:\n");
        for (String key : averagePriceMap.keySet()) {
            System.out.println("Short zip code: " + key);
            System.out.println("Average rent price per meter: " + averagePriceMap.get(key) + "\n");
        }
    }

    public void printAverageSalePricePerMeter() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForSale();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        double sumSalePerMeter = 0;
        Map<String, Double> averagePriceMap = new TreeMap<String, Double>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumSalePerMeter += asset.getSalePrice() / asset.getSize();
            }
            averagePriceMap.put(key, sumSalePerMeter / rentAssetsByShortZipCode.get(key).size());
            sumSalePerMeter = 0;
        }

        System.out.println("Average sale prices per meter:\n");
        for (String key : averagePriceMap.keySet()) {
            System.out.println("Short zip code: " + key);
            System.out.println("Average sale price per meter: " + averagePriceMap.get(key).intValue() + "\n");
        }
    }

    private Map<String, Double> getRentPricePerMeterByZipCode() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForRent();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        double sumRentPerMeter = 0;
        Map<String, Double> averagePriceMap = new TreeMap<String, Double>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumRentPerMeter += asset.getRentPrice() / asset.getSize();
            }
            averagePriceMap.put(key, sumRentPerMeter / rentAssetsByShortZipCode.get(key).size());
            sumRentPerMeter = 0;
        }
        return averagePriceMap;
    }

    private Map<String, Double> getSalePricePerMeterByZipCode() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForSale();
        Map<String, List<Asset>> rentAssetsByShortZipCode = new HashMap<String, List<Asset>>();
        List<Asset> currentList = new ArrayList<Asset>();
        currentList.add(rentAssets.get(0));
        for (int i = 0; i < rentAssets.size() - 1; i++) {
            if (!rentAssets.get(i).getShortZipCode().equals(rentAssets.get(i + 1).getShortZipCode())) {
                rentAssetsByShortZipCode.put(rentAssets.get(i).getShortZipCode(), currentList);
                currentList = new ArrayList<Asset>();
            }
            currentList.add(rentAssets.get(i + 1));
        }
        rentAssetsByShortZipCode.put(rentAssets.get(rentAssets.size() - 1).getShortZipCode(), currentList);

        double sumSalePerMeter = 0;
        Map<String, Double> averagePriceMap = new TreeMap<String, Double>();
        for (String key : rentAssetsByShortZipCode.keySet()) {
            for (Asset asset : rentAssetsByShortZipCode.get(key)) {
                sumSalePerMeter += asset.getSalePrice() / asset.getSize();
            }
            averagePriceMap.put(key, sumSalePerMeter / rentAssetsByShortZipCode.get(key).size());
            sumSalePerMeter = 0;
        }
        return averagePriceMap;
    }

    public void printBestRatioBetweenSaleToRentPricePerMeter() {
        Map<String, Double> salePricePerMeter = getSalePricePerMeterByZipCode();
        Map<String, Double> rentPricePerMeter = getRentPricePerMeterByZipCode();
        TreeMap<Double, String> bestPricePerMeter = new TreeMap<Double, String>();
        for (String key : salePricePerMeter.keySet()) {
            if (rentPricePerMeter.get(key) == null) {
                System.out.println("Bad data " + key);
            } else {
                bestPricePerMeter.put(salePricePerMeter.get(key) / rentPricePerMeter.get(key), key);
            }
//            System.out.println("Short zip code: " + key);
//            System.out.println("Average sale price per meter: " + averagePriceMap.get(key).intValue() + "\n");
        }
        System.out.println("Best sale to rent prices per meter by zip code:\n");
        for (Double key : bestPricePerMeter.keySet()) {
            System.out.println("Short zip code: " + bestPricePerMeter.get(key));
            System.out.println("Sale to rent price per meter ratio: " + key.intValue() + "\n");
        }
    }

    public void printRentAndSaleAssetsForZipCodePerMeter(String zipCode) {
        List<Asset> rentAssets = new ArrayList<Asset>();
        List<Asset> saleAssets = new ArrayList<Asset>();
        List<Asset> mixedAssets = assetController.retrieveAllAssetsByShortZipCode(zipCode);
        for (Asset asset : mixedAssets) {
            if (asset.getRentPrice() > 0) {
                rentAssets.add(asset);
            }
            if (asset.getSalePrice() > 0) {
                saleAssets.add(asset);
            }
        }
        System.out.println("Number of rent assets: " + rentAssets.size());
        double rentPricePerMeter = 0, salePricePerMeter = 0;
        for (Asset asset : rentAssets) {
            rentPricePerMeter += asset.getRentPrice() / asset.getSize();
            System.out.println("Asset rent price: " + asset.getRentPrice());
        }
        System.out.println("Number of rent assets: " + saleAssets.size());
        for (Asset asset : saleAssets) {
            salePricePerMeter += asset.getSalePrice() / asset.getSize();
            System.out.println("Asset sale price: " + asset.getSalePrice());
        }
        System.out.println("Average asset rent price: " + rentPricePerMeter / rentAssets.size());
        System.out.println("Average asset sale price: " + salePricePerMeter / saleAssets.size());
        System.out.println("Asset sale to rent price ratio: " + (salePricePerMeter / saleAssets.size()) / (rentPricePerMeter / rentAssets.size()));

    }

    public List<Asset> retrieveAllRentAssetsAscByGroupZipCode() {
        return assetController.getAllRentAssetsByGroupZipCodeAsc();
    }

    public void printSpmRpmRatioByGroupZipCode() {
        List<Asset> rentAssets, saleAssets;
        rentAssets = assetController.getAllRentAssetsByGroupZipCodeAsc();
//        saleAssets = assetController.get

    }

    // deleting methods, do not use unless needed
    public void updateAllAssetsWithReasonableRentPrice() {
        List<Asset> rentAssets = assetController.retrieveAllAssetsForRent();
        List<Asset> newRentAssets = new ArrayList<Asset>();
        int price;
        for (Asset asset : rentAssets) {
            price = asset.getRentPrice();
            while (price > 10000) {
                price /= 10;
            }
            asset.setRentPrice(price);
            newRentAssets.add(asset);
        }
        assetController.deleteAllAssets();
        assetController.insertAssets(newRentAssets);
    }

    public void updateAllAssetsWithGroupShortZipCode() {
        List<Asset> allAssets = assetController.retrieveAllAssets();
        for (Asset asset : allAssets) {
            asset.setGroupZipCode(asset.getShortZipCode().substring(0, 3));
        }
        assetController.insertAssets(allAssets);
    }

    public void checkAvailability() {
        Date date = Calendar.getInstance().getTime();
        List<Asset> assets = getAllAssets();
        for (Asset asset : assets) {
            if (date.getTime() > asset.getLastUpdate().getTime() + Constants.DAY_IN_MILIS) {
                asset.setAvailability(false);
            }
        }
        assetController.insertAssets(assets);
    }

    public void deleteAllWithZeroArea() {
        List<Asset> rentAssets = assetController.getAllWithNoArea();
        assetController.deleteAllAssets(rentAssets);
    }

    public List<Asset> getAllRentAssets() {
        return assetController.retrieveAllAssetsForRent();
    }

    public List<Asset> getAllSaleAssets() {
        return assetController.retrieveAllAssetsForSale();
    }

    public List<Asset> getAllRentAndSaleAssets() {
        return assetController.retrieveAllAssetsWithRentAndSalePrice();
    }

    public List<Asset> getAllAssets() {
        return assetController.retrieveAllAssets();
    }


    public List<Asset> getAllRentAssetsInGroupZipCode(String groupZipCode) {
        return assetController.getAllRentAssetsInGroupZipCode(groupZipCode, true);

    }

    public List<Asset> getAllSaleAssetsInGroupZipCode(String groupZipCode) {
        return assetController.getAllSaleAssetsInGroupZipCode(groupZipCode, true);

    }

    public void updateAllAssetsWithListedSinceDate() {
        List<Asset> assets = assetController.retrieveAllAssets();
        Elements dataTable;
        Document doc;
        for (Asset asset : assets) {
            if (asset.getListedSince() == -1) {
                try {
                    doc = Jsoup.connect(asset.getUrl()).get();
                    dataTable = doc.select(".object-kenmerken-list");
                    asset.setListedSince(ScraperUtils.calculateListedSince(dataTable));
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        assetController.insertAssets(assets);
    }
}
