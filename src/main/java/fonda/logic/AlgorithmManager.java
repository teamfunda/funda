package fonda.logic;

import fonda.pojo.Asset;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Yuval on 12/15/2016.
 */
public class AlgorithmManager {

    private final int NUMBER_OF_TESTS = 10;

    public  Algorithm getBestAlgorithm(List<Asset> assets, boolean isListOfSaleAssets) {
        double dif = Double.MAX_VALUE;
        Algorithm algorithm = null;
        Pair<Double, Algorithm> pair;

        // check each algorithm
        for (AlgorithmType type : AlgorithmType.values()) {
            for (int i = 0; i < NUMBER_OF_TESTS; i++) {
                pair = getDifferenceByPercent(assets, isListOfSaleAssets, type);
                System.out.println(type + " dif is: " + pair.getKey());
                if (Math.abs(pair.getKey()) < Math.abs(dif)) {
                    algorithm = pair.getValue();
                    algorithm.setDif(pair.getKey());
                    dif = pair.getKey();
                }
            }
        }

        System.out.println("The best dif is: " + dif);
        return algorithm;
    }

    public Pair<Double, Algorithm> getDifferenceByPercent(List<Asset> assets, boolean isListOfSaleAssets, AlgorithmType type) {

        List<Asset> studyList = new ArrayList<Asset>();
        List<Asset> testList = new ArrayList<Asset>();


        Collections.shuffle(assets);
        // fill the lists
        for (int i = 0; i < assets.size() * 0.9; i++)
            studyList.add(assets.get(i));
        for (int i = (int) (assets.size() * 0.9); i < assets.size(); i++)
            testList.add(assets.get(i));

        Algorithm algorithm;
        if (isListOfSaleAssets)
            algorithm = new Algorithm(null, assets, type);
        else
            algorithm = new Algorithm(assets, null, type);


        // compares estimations with actual prices
        double totalPrice = 0;
        double totalDifference = 0;
        for (Asset asset : testList) {
            int realPrice;
            if (isListOfSaleAssets) {
                realPrice = asset.getSalePrice();
            } else {
                realPrice = asset.getRentPrice();
            }
            totalPrice += realPrice;

            double estimation = algorithm.getEstimatedPrice(asset, isListOfSaleAssets);
            if (estimation != -1) {
                double difference = realPrice - estimation;
                totalDifference += difference;
            }
        }

        System.out.println("data size: " + studyList.size());
//        System.out.println("sale difference is: " + totalDifference / totalPrice + "%");
        return new Pair<>(totalDifference / totalPrice * 100, algorithm);
    }
}
