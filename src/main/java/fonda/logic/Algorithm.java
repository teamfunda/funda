package fonda.logic;

import fonda.pojo.Asset;
import org.apache.commons.math3.stat.regression.OLSMultipleLinearRegression;

import java.util.List;

/**
 * Created by Yuval on 12/13/2016.
 */
public class Algorithm {

    private int NUMBER_OF_PARAMETERS = 5;
    private double[] saleCoes;
    private double[] rentCoes;
    private AlgorithmType type;
    private double dif;

    public Algorithm(List<Asset> rentAssets, List<Asset> saleAssets, AlgorithmType type) {
        this.type = type;

        if(saleAssets!=null){
            saleCoes = getCoes(saleAssets, true);

        }
        if(rentAssets!=null){
            rentCoes = getCoes(rentAssets, false);

        }
    }

    public Algorithm(double[] coes, boolean isCoesOfSale){
        if(isCoesOfSale)
            this.saleCoes = coes;
        else
            this.rentCoes = coes;
    }

    private double[] getCoes(List<Asset> assets, boolean isEstimationForSale) {

        OLSMultipleLinearRegression regression = new OLSMultipleLinearRegression();
        double[][] parameters;
        switch (type) {
            case ALL_PARAMS:
                parameters = new double[assets.size()][NUMBER_OF_PARAMETERS];
                break;

            case EXCLUDE_PRICE:
                parameters = new double[assets.size()][NUMBER_OF_PARAMETERS - 1];
                break;

            default:
                parameters = new double[assets.size()][NUMBER_OF_PARAMETERS];
                break;
        }

        double[] results = new double[assets.size()];

        for (int i = 0; i < assets.size(); i++) {
            if (isEstimationForSale) {
                results[i] = assets.get(i).getSalePrice();
                if (type == AlgorithmType.ALL_PARAMS) {
                    parameters[i][NUMBER_OF_PARAMETERS - 1] = assets.get(i).getRentPrice();

                }

            } else {
                results[i] = assets.get(i).getRentPrice();
                if (type == AlgorithmType.ALL_PARAMS) {
                    parameters[i][NUMBER_OF_PARAMETERS - 1] = assets.get(i).getSalePrice();

                }
            }

            parameters[i][0] = assets.get(i).getSize();
            parameters[i][1] = assets.get(i).getNumberOfRooms();
            parameters[i][2] = assets.get(i).getNumberOfBedRooms();
            parameters[i][3] = assets.get(i).getConstructionYear();
//            parameters[i][4] = assets.get(i).getListedSince();

        }
        regression.newSampleData(results, parameters);

//        printMatrix(parameters, results);

        double[] coes = regression.estimateRegressionParameters();

        return coes;

    }

    private void printMatrix(double[][] x, double[] y) {
        for (int i = 0; i < x.length; i++) {
            System.out.print(y[i] + " = ");
            for (int j = 0; j < x[0].length; j++) {
                System.out.print(x[i][j] + " + ");
            }
            System.out.println();
        }
    }

    public double getEstimatedPrice(Asset asset, boolean isEstimationForSale) {
        //checking if the method being called is not a mistake
        if (asset.getSalePrice() != 0 && asset.getRentPrice() != 0) {
//            System.out.println("The asset has both rent and sale prices");
            return -1;
        }


        final double[] params;
        if (isEstimationForSale) {
            params = saleCoes;
        } else {
            params = rentCoes;
        }

//        System.out.println(Arrays.toString(params));
        double[] values = fillValues(isEstimationForSale, asset);
        return calculateValue(values, params);

    }

    private double[] fillValues(boolean isEstimationForSale, Asset asset) {
        double[] values;
        if(type == AlgorithmType.ALL_PARAMS){
            values = new double[NUMBER_OF_PARAMETERS];
            if (isEstimationForSale)
                values[NUMBER_OF_PARAMETERS - 1] = asset.getRentPrice();
            else
                values[NUMBER_OF_PARAMETERS - 1] = asset.getSalePrice();
        }
        else{
            values = new double[NUMBER_OF_PARAMETERS-1];
        }


        values[0] = asset.getSize();
        values[1] = asset.getNumberOfRooms();
        values[2] = asset.getNumberOfBedRooms();
        values[3] = asset.getConstructionYear();
//        values[4] = asset.getListedSince();

        return values;
    }


    private double calculateValue(double[] x, double[] coe) {

        double sum = coe[0];
        for (int i = 0; i < x.length; i++) {
            sum += x[i] * coe[i + 1];
        }
        return sum;
    }

    public double getDif(){
        return this.dif;
    }
    public void setDif(double dif){
        this.dif = dif;
    }


}
