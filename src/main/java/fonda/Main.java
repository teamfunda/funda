package fonda;

import fonda.logic.Algorithm;
import fonda.logic.AlgorithmManager;
import fonda.logic.DataManager;
import fonda.pojo.Asset;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class Main {


    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(Main.class);
        springApplication.setWebEnvironment(false);
        ApplicationContext context = springApplication.run(args);
        DataManager dataManager = new DataManager(context);
//
//        Scraper scraper = new Scraper();
//        scraper.startScraping(context);
//        dataManager.checkAvailability(); //IMPORTANT! inefficient until the scraper has no timeouts
//        dataManager.updateAllAssetsWithListedSinceDate();
//

        printSuggestions(dataManager, false);

    }

    public static void printSuggestions(DataManager dataManager, boolean isForSale) {
        List<Asset> rentAssets = dataManager.getAllRentAssets();
        List<Asset> saleAssets = dataManager.getAllSaleAssets();
        List<Asset> assets;

        AlgorithmManager algorithmManager = new AlgorithmManager();
        Algorithm algorithm;
        if (isForSale) {
            algorithm = algorithmManager.getBestAlgorithm(saleAssets, isForSale);
            assets = rentAssets;
        } else {
            algorithm = algorithmManager.getBestAlgorithm(rentAssets, isForSale);
            assets = saleAssets;
        }


        // set estimations to all of the assets
        for (Asset asset : assets) {
            double estimation = algorithm.getEstimatedPrice(asset, isForSale) * (1 + (algorithm.getDif() / 100));
            if (isForSale) {
                if (asset.getSalePrice() == 0)
                    asset.setSalePrice((int) estimation);
            } else {
                if (asset.getRentPrice() == 0)
                    asset.setRentPrice((int) estimation);
            }

        }

        Collections.sort(assets);

        for (int i=0; i<assets.size(); i++) {
            System.out.println(assets.get(i).toString());
            System.out.println("ratio is: " + (double) assets.get(i).getSalePrice() / assets.get(i).getRentPrice());
        }
//            Asset asset = getMockAsset();
//        System.out.println("First estimation is: " + algorithm.getEstimatedPrice(asset, isForSale));
//        System.out.println("best algorithm is: " + type);
//
//        System.out.println("Final estimation is: " + algorithm.getEstimatedPrice(asset, isForSale) * (1 + (algorithm.getDif() / 100)));
    }


    private static Asset getMockAsset() {
        Asset asset = new Asset();
        asset.setId("49981532");
        asset.setAddress("Botterstraat 138");
        asset.setSalePrice(179000);
        asset.setRentPrice(0);
        asset.setAvailability(true);
        asset.setFullZipCode("1034 BV Amsterdam");
        asset.setShortZipCode("1034");
        asset.setGroupZipCode("103");
        asset.setSize(76);
        asset.setUrl("http://www.funda.nl/en/koop/amsterdam/appartement-49981532-botterstraat-138/");
        asset.setConstructionYear(1967);
        asset.setNumberOfRooms(3);
        asset.setNumberOfBedRooms(2);
        asset.setListedSince(60);
        asset.setLastUpdate(Calendar.getInstance().getTime());

        return asset;
    }
}
