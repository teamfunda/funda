package fonda.knn;

import fonda.pojo.Asset;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ofekrom on 10/12/2016.
 */
public class MyDataSet {
    /**********
     * Variables
     *************/

    List<Example> dataSet = new ArrayList<Example>();


    private int attributeNum;

    /**********
     * Methods
     ***************/


    public MyDataSet(List<Asset> assets, int attributeNum) {
        this.attributeNum = attributeNum;
        double attributes[] = new double[attributeNum];

        // read in the each of the example
        for (int k = 0; k < assets.size(); k++) {
            attributes[0] = assets.get(k).getSize();
            attributes[1] = Double.parseDouble(assets.get(k).getShortZipCode());
            dataSet.add(new Example(0, attributes));
        }
    }


    /**
     * Gets a particular example
     *
     * @param index of the example to retrieve
     **/
    public Example getDataObject(int index) {
        return dataSet.get(index);
    }

    /**
     * Accessor for the classLabel
     *
     * @return classLabel for example index
     **/
    public int getClassLabel(int index) {
        return getDataObject(index).getClassLabel();
    }

    /**
     * Accessor for attributes
     *
     * @param index contains the numbering for the example in the dataset
     * @param j     contains the numbering for the attribute
     * @return an attribute of example i attribute j
     **/
    public double getAttribute(int index, int j) {
        return getDataObject(index).getAttribute(j);
    }

    public int getAttributeNum() {
        return attributeNum;
    }

    public void setAttributeNum(int attributeNum) {
        this.attributeNum = attributeNum;
    }

}
