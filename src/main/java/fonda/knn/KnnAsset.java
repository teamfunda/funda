package fonda.knn;

import java.util.StringTokenizer;

/**
 * Created by ofekrom on 10/12/2016.
 */
public class KnnAsset extends kNNExample {
    /**
     * Constructor
     *
     * @param example
     * @param id
     */
    public KnnAsset(Example example, int id) {
        super(example, id);
    }
}
