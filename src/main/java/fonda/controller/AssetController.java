package fonda.controller;

import fonda.pojo.Asset;
import fonda.pojo.AssetDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AssetController {

    @Autowired
    private AssetDao assetDao;

    public void insertAssets(List<Asset> assets) {
        assetDao.save(assets);
    }

    public void insertAsset(Asset asset){
        assetDao.save(asset);
    }

    public void deleteAsset(Asset asset) {
        assetDao.delete(asset);
    }

    public void deleteAllAssets() {
        assetDao.deleteAll();
    }

    public void deleteAllAssets(List<Asset> assets) {
        assetDao.delete(assets);
    }

    public List<Asset> retrieveAllAssets() {
        return (List<Asset>) assetDao.findAll();
    }

    public List<Asset> retrieveAllAssetsWithRentAndSalePrice() {
        return assetDao.findByRentPriceGreaterThanAndSalePriceGreaterThan(0, 0);
    }

    public List<Asset> retrieveAllAssetsForRent() {
        return removeAllAssetsWithoutSize(assetDao.findByRentPriceGreaterThanOrderByShortZipCodeAsc(0));
    }


    public List<Asset> retrievAllAssetsForShortZipCodeRange(String startShortZipCode, String endShortZipCode) {
        return null;
    }

    public List<Asset> retrieveAllAssetsByShortZipCode(String shortZipCode) {
        return assetDao.findByShortZipCode(shortZipCode);
    }

    public List<Asset> retrieveAllAssetsForSale() {
        return assetDao.findBySalePriceGreaterThanOrderByShortZipCodeAsc(0);
    }

    private List<Asset> removeAllAssetsWithoutSize(List<Asset> assets) {
        List<Asset> validAssets = new ArrayList<Asset>();
        for (Asset asset : assets) {
            if (asset.getSize() >= 18) {
                validAssets.add(asset);
            }
        }
        return validAssets;
    }

    private void printAllAssets(Iterable<Asset> assets) {
        for (Asset asset : assets) {
            System.out.println(asset);
        }
    }

    public List<Asset> getAllWithNoArea() {
        return assetDao.findBySizeEquals(0);
    }

    public List<Asset> getAllRentAssetsByGroupZipCodeAsc() {
        return assetDao.findByRentPriceGreaterThanOrderByGroupZipCodeAsc(0);
    }

    public List<Asset> getAllSaleAssetsByGroupZipCodeAsc() {
        return assetDao.findBySalePriceGreaterThanOrderByGroupZipCodeAsc(0);
    }



    public List<Asset> getAllRentAssetsInGroupZipCode(String groupZipCode, boolean availability) {
        return assetDao.findByGroupZipCodeEqualsAndRentPriceGreaterThanAndAvailabilityIs(groupZipCode,0, availability);
    }

    public List<Asset> getAllSaleAssetsInGroupZipCode(String groupZipCode, boolean availability) {
        return assetDao.findByGroupZipCodeEqualsAndSalePriceGreaterThanAndAvailabilityIs(groupZipCode,0, availability);

    }
}
