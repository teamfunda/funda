package fonda.scraper;

import fonda.utils.Constants;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

/**
 * Created by ofekrom on 16/12/2016.
 */
public class ScraperProducer extends Thread {
    private ScrapingManager mScrapingManager;
    private String mBaseUrl;
    private int mStartPage;
    private int mEndPage;

    public ScraperProducer(ScrapingManager scrapingManager, String baseUrl, int startPage, int endPage) {
        mScrapingManager = scrapingManager;
        mBaseUrl = baseUrl;
        mStartPage = startPage;
        mEndPage = endPage;
    }

    public void run() {
        Document document;
        String detailsViewUrl;
        String finalUrl;
        for (int i = mStartPage; i <= mEndPage; i++) {
            try {
                System.out.println("Page " + i);
                document = Jsoup.connect(mBaseUrl + "/p" + i + "/").get();
                for (Element row : document.select("div.search-result-content-inner")) {
                    detailsViewUrl = Jsoup.parse(row.select(".search-result-header").toString()).select("a").first().attr("href");
                    finalUrl = Constants.FUNDA_BASE_URL + detailsViewUrl;
                    System.out.println("Added to Queue " + finalUrl);
                    mScrapingManager.addUrlToQueue(finalUrl);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mScrapingManager.updateProducerThreadIsDead();
    }
}