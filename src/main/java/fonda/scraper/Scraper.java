package fonda.scraper;

import fonda.Main;
import fonda.controller.AssetController;
import fonda.pojo.Asset;
import fonda.utils.Constants;
import fonda.utils.ScraperUtils;
import fonda.utils.Utils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Yuval on 11/27/2016.
 */
public class Scraper {

    private List<Asset> pendingAssets;

    public void startScraping(ApplicationContext context) {
        try {
            pendingAssets = new ArrayList<Asset>();
            System.out.println("All rent assets");
            addAllData(Constants.FUNDA_BASE_URL + Constants.FUNDA_RENT_SEARCH_URL);
//            System.out.println("All sale assets");
//            addAllData(Constants.FUNDA_BASE_URL + Constants.FUNDA_SALE_SEARCH_URL);
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            AssetController assetController = context.getBean(AssetController.class);
            assetController.insertAssets(pendingAssets);
        }
    }

    private void addAllData(String url) {
        try {
            Connection connection = Jsoup.connect(url);
            connection.timeout(50000); // timeout in millis
            Document doc = connection.get();
            Element paginationElement = doc.select("div.pagination-numbers").get(0);
            Element last = (Element) paginationElement.childNodes().get(paginationElement.childNodeSize() - 2);
            int lastPage = Integer.parseInt(last.attr("data-pagination-page"));
            doAddAllData(doc, 0);
            for (int i = 1; i <= lastPage; i++) {
                doc = Jsoup.connect(url + "/p" + i + "/").get();
                doAddAllData(doc, i);
            }
        } catch (Exception e) {
            Logger.getLogger(Main.class.getName(), e.toString());
        }
    }

    private void doAddAllData(Document document, int page) throws IOException {
        String rentPrice, salePrice, status, address, url, id, apartmentSize, detailsViewUrl, fullZipCode, shortZipCode, alternateApartmentSize;
        Integer constructionYear, alternateConstructionYear, numberOfRooms, numberOfBedRooms, listedSince;
        Elements dataTable;
        Document doc;
        System.out.println("Page " + page + "\n");
        Asset apartment;
        for (Element row : document.select("div.search-result-content-inner")) {
            detailsViewUrl = Jsoup.parse(row.select(".search-result-header").toString()).select("a").first().attr("href");
            url = Constants.FUNDA_BASE_URL + detailsViewUrl;
            id = String.valueOf(Utils.extractAllNumbersFromString(detailsViewUrl));
            doc = Jsoup.connect(url).get();
            address = doc.select(".object-header-title").text();
            fullZipCode = doc.select(".object-header-subtitle").text();
            shortZipCode = String.valueOf(Utils.extractAllNumbersFromString(fullZipCode));
            dataTable = doc.select(".object-kenmerken-list");
            rentPrice = ScraperUtils.getTextFromddOrErrorMessage(Constants.RENTAL_PRICE_ELEMENT, dataTable);
            salePrice = ScraperUtils.getTextFromddOrErrorMessage(Constants.SALE_PRICE_ELEMENT, dataTable);
            status = ScraperUtils.getTextFromddOrErrorMessage(Constants.STATUS_ELEMENT, dataTable);
            listedSince = ScraperUtils.calculateListedSince(dataTable);
            alternateConstructionYear = ScraperUtils.getIntegerFromddOrNegative(Constants.YEAR_OF_CONSTRUCTION_ALTERNATE, dataTable);
            constructionYear = ScraperUtils.getConstructionYear(dataTable, alternateConstructionYear);
            int[] arr = ScraperUtils.calculateNumberOfRoomsAndBedRooms(dataTable);
            numberOfRooms = arr[0];
            numberOfBedRooms = arr[1];
            alternateApartmentSize = ScraperUtils.getTextFromddOrNull(Constants.AREA_ELEMENT, dataTable);
            dataTable = doc.select(".object-kenmerken-group-list");
            apartmentSize = ScraperUtils.getTextForArea(dataTable, alternateApartmentSize);
            if (apartmentSize != null && numberOfRooms > 0 && ScraperUtils.isAssetAnApartment(doc)) {
                apartment = new Asset(id, address, url, Utils.extractFirstNumberFromString(salePrice), Utils.extractFirstNumberFromString(rentPrice), Utils.extractFirstNumberFromString(apartmentSize), true, fullZipCode, shortZipCode, listedSince, constructionYear, numberOfRooms, numberOfBedRooms);
                pendingAssets.add(apartment);
                System.out.println(apartment.toString());
            } else {
                System.out.println("Asset not an apartment");
            }
        }
    }
}

