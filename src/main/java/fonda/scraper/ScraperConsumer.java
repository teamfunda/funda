package fonda.scraper;

import fonda.pojo.Asset;
import fonda.utils.Constants;
import fonda.utils.ScraperUtils;
import fonda.utils.Utils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by ofekrom on 16/12/2016.
 */
public class ScraperConsumer extends Thread {

    private ScrapingManager mScrapingManager;
    private int retries;
    private static final int MAX_CONSUMING_RETRIES = 5;

    public ScraperConsumer(ScrapingManager scrapingManager) {
        mScrapingManager = scrapingManager;
    }

    public void run() {
        String nextUrl;
        while (true) {
            nextUrl = mScrapingManager.pollUrl();
            if (nextUrl != null) {
                try {
                    buildAsset(nextUrl);
                } catch (IOException e) {
                    mScrapingManager.updateProblematicURL(nextUrl);
                }
            } else {
                retries++;
                if (retries <= MAX_CONSUMING_RETRIES) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    mScrapingManager.updateConsumerThreadIsDead();
                    return;
                }
            }
        }
    }


    private void buildAsset(String url) throws IOException {
        String rentPrice, salePrice, status, address, id, apartmentSize, fullZipCode, shortZipCode, alternateApartmentSize;
        Integer constructionYear, alternateConstructionYear, numberOfRooms, numberOfBedRooms, listedSince;
        Elements dataTable;
        Document doc;
        Asset apartment;
        id = String.valueOf(Utils.extractFirstNumberFromString(url));
        doc = Jsoup.connect(url).get();
        address = doc.select(".object-header-title").text();
        fullZipCode = doc.select(".object-header-subtitle").text();
        shortZipCode = String.valueOf(Utils.extractAllNumbersFromString(fullZipCode));
        dataTable = doc.select(".object-kenmerken-list");
        rentPrice = ScraperUtils.getTextFromddOrErrorMessage(Constants.RENTAL_PRICE_ELEMENT, dataTable);
        salePrice = ScraperUtils.getTextFromddOrErrorMessage(Constants.SALE_PRICE_ELEMENT, dataTable);
        status = ScraperUtils.getTextFromddOrErrorMessage(Constants.STATUS_ELEMENT, dataTable);
        listedSince = ScraperUtils.calculateListedSince(dataTable);
        alternateConstructionYear = ScraperUtils.getIntegerFromddOrNegative(Constants.YEAR_OF_CONSTRUCTION_ALTERNATE, dataTable);
        constructionYear = ScraperUtils.getConstructionYear(dataTable, alternateConstructionYear);
        int[] arr = ScraperUtils.calculateNumberOfRoomsAndBedRooms(dataTable);
        numberOfRooms = arr[0];
        numberOfBedRooms = arr[1];
        alternateApartmentSize = ScraperUtils.getTextFromddOrNull(Constants.AREA_ELEMENT, dataTable);
        dataTable = doc.select(".object-kenmerken-group-list");
        apartmentSize = ScraperUtils.getTextForArea(dataTable, alternateApartmentSize);
        if (apartmentSize != null && numberOfRooms > 0 && ScraperUtils.isAssetAnApartment(doc)) {
            apartment = new Asset(id, address, url, Utils.extractFirstNumberFromString(salePrice), Utils.extractFirstNumberFromString(rentPrice), Utils.extractFirstNumberFromString(apartmentSize), true, fullZipCode, shortZipCode, listedSince, constructionYear, numberOfRooms, numberOfBedRooms);
            mScrapingManager.addAsset(apartment);
            System.out.println("Asset added");
//            System.out.println(apartment.toString());
        } else {
            System.out.println("Asset not an apartment");
        }
    }
}
