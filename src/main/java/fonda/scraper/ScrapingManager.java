package fonda.scraper;

import fonda.controller.AssetController;
import fonda.pojo.Asset;
import fonda.utils.Constants;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by ofekrom on 16/12/2016.
 */


public class ScrapingManager {
    private static final int MAX_URLS = 2000;
    private BlockingQueue<String> queue;
    private final List<Asset> pendingAssets = new LinkedList<>();
    private ApplicationContext context;
    private static final int NUM_OF_CONSUMER_THREADS = 6;
    private static final int NUM_OF_PRODUCER_THREADS = 2;
    private final ScrapingDoneJobsLock doneJobsLock = new ScrapingDoneJobsLock();
    private final List<String> problematicUrls = new LinkedList<>();
    private ExecutorService executorService = Executors.newCachedThreadPool();

    public void startScraping(ApplicationContext context) {
        this.context = context;
        queue = new ArrayBlockingQueue<String>(MAX_URLS, false);
        String baseUrl = Constants.FUNDA_BASE_URL + Constants.FUNDA_RENT_SEARCH_URL;
        Connection connection = Jsoup.connect(baseUrl);
        connection.timeout(50000); // timeout in millis
        Document doc = null;
        try {
            doc = connection.get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Element paginationElement = doc.select("div.pagination-numbers").get(0);
        Element last = (Element) paginationElement.childNodes().get(paginationElement.childNodeSize() - 2);
        int lastPage = Integer.parseInt(last.attr("data-pagination-page"));
        for (int i = 0; i < NUM_OF_PRODUCER_THREADS; i++) {
            executorService.submit(new ScraperProducer(this, baseUrl, i * (lastPage / 2), (i + 1) * (lastPage / 2)));
            doneJobsLock.numberOfLiveProducerThreads++;
        }
        for (int i = 0; i < NUM_OF_CONSUMER_THREADS; i++) {
            executorService.submit(new ScraperConsumer(this));
            doneJobsLock.numberOfLiveConsumerThreads++;
        }
    }

    public void addUrlToQueue(String url) {
        queue.add(url);
    }

    public String pollUrl() {
        return queue.poll();
    }

    public void updateProblematicURL(String url) {
        synchronized (problematicUrls) {
            problematicUrls.add(url);
        }
    }

    public int queueSize() {
        return queue.size();
    }

    public void addAsset(Asset asset) {
        synchronized (pendingAssets) {
            pendingAssets.add(asset);
        }
    }

    public void updateConsumerThreadIsDead() {
        synchronized (doneJobsLock) {
            doneJobsLock.numberOfLiveConsumerThreads--;
            System.out.println("Consumer thread dead");
            if (doneJobsLock.allJobsDone()) {
                finishScraping();
            }
        }
    }

    public void updateProducerThreadIsDead() {
        synchronized (doneJobsLock) {
            doneJobsLock.numberOfLiveProducerThreads--;
            System.out.println("Producer thread dead");
            if (doneJobsLock.allJobsDone()) {
                finishScraping();
            }
        }
    }

    private void finishScraping() {
        uploadAllAssets();
        printAllProblematicUrls();

    }

    public void uploadAllAssets() {
        if (!pendingAssets.isEmpty()) {
            AssetController assetController = context.getBean(AssetController.class);
            assetController.insertAssets(pendingAssets);
        }

        pendingAssets.clear();
    }

    private void printAllProblematicUrls() {
        System.out.println("Problematic urls:\n");
        for (String url : problematicUrls) {
            System.out.println(url);
        }
    }


    private class ScrapingDoneJobsLock {

        private Integer numberOfLiveConsumerThreads = 0;
        private Integer numberOfLiveProducerThreads = 0;

        private boolean allJobsDone() {
            return numberOfLiveConsumerThreads == 0 && numberOfLiveProducerThreads == 0;
        }
    }
}


