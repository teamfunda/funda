package fonda.pojo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ofekrom on 30/11/2016.
 */

@Transactional
public interface AssetDao extends CrudRepository<Asset, String> {


    List<Asset> findByShortZipCode(String shortZipCode);

    List<Asset> findByShortZipCodeBetweenOrderByShortZipCodeDesc(String startShortZipCode, String endShortZipCode);

    List<Asset> findByRentPriceGreaterThanOrderByShortZipCodeAsc(int rentPrice);

    List<Asset> findBySalePriceGreaterThanOrderByShortZipCodeAsc(int salePrice);

    List<Asset> findByRentPriceGreaterThanOrderByGroupZipCodeAsc(int rentPrice);

    List<Asset> findBySalePriceGreaterThanOrderByGroupZipCodeAsc(int salePrice);

    List<Asset> findBySizeEquals(int size);

    List<Asset> findByRentPriceGreaterThanAndSalePriceGreaterThan(int rentPrice, int salePrice);

    List<Asset> findByGroupZipCodeEqualsAndRentPriceGreaterThanAndAvailabilityIs(String groupZipCode, int rentPrice, boolean availability);
    List<Asset> findByGroupZipCodeEqualsAndSalePriceGreaterThanAndAvailabilityIs(String groupZipCode, int salePrice, boolean availability);

}