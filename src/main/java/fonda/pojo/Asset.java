package fonda.pojo;

import fonda.utils.Constants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "assets", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")})


public class Asset implements Serializable, Comparable<Asset> {

    private static final long serialVersionUID = -1798070786993154676L;


    @Id
    @Column(name = "id", unique = true, nullable = false)
    private String id;


    @Column(name = "address", unique = false, nullable = false)
    private String address;


    @Column(name = "url", unique = true, nullable = false)
    private String url;

    @Column(name = "sale_price", unique = false, nullable = true)
    private Integer salePrice;

    @Column(name = "rent_price", unique = false, nullable = true)
    private Integer rentPrice;

    @Column(name = "size", unique = false, nullable = true)
    private Integer size;

    @Column(name = "availability", unique = false, nullable = false)
    private Boolean availability;

    @Column(name = "created", unique = false, nullable = true)
    private Date dateCreated;

    @Column(name = "last_update", unique = false, nullable = true)
    private Date lastUpdate;

    @Column(name = "full_zip_code", unique = false, nullable = true)
    private String fullZipCode;

    @Column(name = "short_zip_code", unique = false, nullable = true)
    private String shortZipCode;

    @Column(name = "group_zip_code", unique = false, nullable = true)
    private String groupZipCode;

    @Column(name = "listed_since_date", unique = false, nullable = true)
    private Date listedSince;

    @Column(name = "construction_year", unique = false, nullable = true)
    private Integer constructionYear;

    @Column(name = "number_of_rooms", unique = false, nullable = true)
    private Integer numberOfRooms;

    @Column(name = "number_of_bed_rooms", unique = false, nullable = true)
    private Integer numberOfBedRooms;

    public Asset(String id, String address, String url, Integer salePrice, Integer rentPrice, Integer size, Boolean availability, String fullZipCode, String shortZipCode, Integer listedSince, Integer constructionYear, Integer numberOfRooms, Integer numberOfBedRooms) {
        this.id = id;
        this.address = address;
        this.url = url;
        this.salePrice = salePrice;
        this.rentPrice = rentPrice;
        this.size = size;
        this.availability = availability;
        this.fullZipCode = fullZipCode;
        this.shortZipCode = shortZipCode;
        this.groupZipCode = shortZipCode.substring(0, 3);
        this.dateCreated = Calendar.getInstance().getTime();
        this.lastUpdate = this.dateCreated;
        this.listedSince = new Date(dateCreated.getTime() - listedSince * Constants.DAY_IN_MILIS);
        this.constructionYear = constructionYear;
        this.numberOfRooms = numberOfRooms;
        this.numberOfBedRooms = numberOfBedRooms;
    }

    public Asset() {

    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Integer salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getRentPrice() {
        return rentPrice;
    }

    public void setRentPrice(Integer rentPrice) {
        this.rentPrice = rentPrice;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Boolean getAvailability() {
        return availability;
    }

    public void setAvailability(Boolean availability) {
        this.availability = availability;
    }

    public Date getDate() {
        return dateCreated;
    }

    public void setDate(Date date) {
        this.dateCreated = date;
    }
//Getters and setters

    public String getFullZipCode() {
        return fullZipCode;
    }

    public void setFullZipCode(String fullZipCode) {
        this.fullZipCode = fullZipCode;
    }

    public String getShortZipCode() {
        return shortZipCode;
    }

    public void setShortZipCode(String shortZipCode) {
        this.shortZipCode = shortZipCode;
    }

    public String getGroupZipCode() {
        return groupZipCode;
    }

    public void setGroupZipCode(String groupZipCode) {
        this.groupZipCode = groupZipCode;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getLastUpdate() {

        return lastUpdate;
    }

    public Integer getListedSince() {
        if (listedSince != null) {
            return (int) (System.currentTimeMillis() - listedSince.getTime() / Constants.DAY_IN_MILIS);
        }
        return -1;
    }

    public void setListedSince(Date listedSince) {
        this.listedSince = listedSince;
    }

    public void setListedSince(int listedSince) {
        this.listedSince = new Date(System.currentTimeMillis() - listedSince * Constants.DAY_IN_MILIS);
    }

    public Integer getConstructionYear() {
        return constructionYear;
    }

    public void setConstructionYear(Integer constructionYear) {
        this.constructionYear = constructionYear;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public Integer getNumberOfBedRooms() {
        return numberOfBedRooms;
    }

    public void setNumberOfBedRooms(Integer numberOfBedRooms) {
        this.numberOfBedRooms = numberOfBedRooms;
    }


    @Override
    public String toString() {
        return ("\nAddress: " + address) +
                ("\nId: " + id) +
                ("\nUrl: " + url) +
                ("\nFull zip code: " + fullZipCode) +
                ("\nShort zip code: " + shortZipCode) +
                ("\nRent price: " + rentPrice) +
                ("\nSale price: " + salePrice) +
                ("\nAvailability: " + availability) +
                ("\nApartment Size: " + size) +
                ("\nAvailability: " + availability) +
                ("\nLast update: " + lastUpdate) +
                ("\nListed since: " + listedSince) +
                ("\nConstruction year: " + constructionYear) +
                ("\nNumber of rooms: " + numberOfRooms) +
                ("\nNumber of bed rooms: " + numberOfBedRooms);


    }

    @Override
    public int compareTo(Asset asset) {
        double currentAsset = (double)this.rentPrice/this.salePrice;
        double otherAsset =(double) asset.rentPrice/asset.salePrice;

        if (currentAsset > otherAsset)
            return 1;
        else
            return -1;

    }
}